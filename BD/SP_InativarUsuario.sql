CREATE PROCEDURE SP_InativarUsuario(@indAtivo char(1), @idUsuario int)
As
BEGIN
UPDATE [dbo].[Usuario]
   SET [indAtivo] = @indAtivo
 WHERE idUsuario = @idUsuario
END