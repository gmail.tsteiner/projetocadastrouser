CREATE PROCEDURE SP_InsereAcessoSistema (@idSistema int, 
										 @idUsuario int, 
										 @idTipoAcessoSistemaUsuario int)

As
Begin

INSERT INTO [dbo].[AcessoSistemaUsuario]([idSistema],[idUsuario],[idTipoAcessoSistemaUsuario])
     VALUES (@idSistema,@idUsuario,@idTipoAcessoSistemaUsuario)
END