CREATE PROCEDURE SP_InsereNovoUsuario (@login varchar(50) =null,
									   @nomeCadastrado varchar(50) =null,
									   @idade int =null,
									   @idEmpresa int =null,
									   @documento varchar(20) =null,
									   @tipodeDocumento varchar(50) =null,
									   @endereco varchar(100) =null,
									   @contato varchar(30) =null,
									   @senha varchar(20) =null,
									   @email varchar(50) =null,
									   @filiacao varchar(50) =null,
									   @tipo varchar(50) =null,
									   @descricao varchar(50) =null,
									   @genero char(1) =null)

As

BEGIN

INSERT INTO [dbo].[Usuario]
           ([login]
           ,[nomeCadastrado]
           ,[idade]
           ,[documento]
           ,[tipodeDocumento]
           ,[endereco]
           ,[contato]
           ,[senha]
           ,[email]
           ,[filiacao]
           ,[tipo]
           ,[descricao]
           ,[genero]
           ,[dataCadastro]
           ,[indAtivo]
		   ,[idEmpresa])
     VALUES
           (@login
           ,@nomeCadastrado
           ,@idade
           ,@documento
           ,@tipodeDocumento
           ,@endereco
           ,@contato
           ,@senha
           ,@email
           ,@filiacao
           ,@tipo
           ,@descricao
           ,@genero
           ,getdate()
           ,'A'
		   ,@idEmpresa)

	SELECT @@IDENTITY

END