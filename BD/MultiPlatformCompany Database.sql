--DROP DATABASE MultiPlatformCompany
--CREATE DATABASE MultiPlatformCompany
use MultiPlatformCompany
go

create table Sistema(
	[idSistema] [int] IDENTITY(1,1) NOT NULL,
	[NomeSistema] [varchar](50) NOT NULL
	CONSTRAINT [PK_Sistema] PRIMARY KEY CLUSTERED ([idSistema] ASC)
);
GO


create table AcessoSistemaUsuario(
    idcessoSistemaUsuario [int] IDENTITY(1,1) NOT NULL,
	[idSistema] int not null,
    [idUsuario] int not null,
	[idTipoAcessoSistemaUsuario] int not null
	CONSTRAINT [PK_AcessoSistemaUsuario] PRIMARY KEY CLUSTERED ([idcessoSistemaUsuario] ASC)
);
GO


CREATE TABLE [Empresa] (
    [idEmpresa]              INT           IDENTITY (1, 1) NOT NULL,
    [NomeEmpresa]             VARCHAR (50)  NOT NULL,
	CONSTRAINT [PK_Empresa] PRIMARY KEY CLUSTERED ([idEmpresa] ASC)	
);
Go

create table TipoAcessoSistemaUsuario(
	[idTipoAcessoSistemaUsuario] [int] IDENTITY(1,1) NOT NULL,
	[nomeAcessoSistemaUsuario] [varchar](50) NOT NULL
	CONSTRAINT [PK_TipoAcessoSistemaUsuario] PRIMARY KEY CLUSTERED ([idTipoAcessoSistemaUsuario] ASC)
);
GO


create table Usuario(
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[login] [varchar](50) NULL,
    [nomeCadastrado] [varchar](50) NULL,
	[nomeAcessoSistemaUsuario] [varchar](50) NULL,
	[idade] [int] NULL,
	[documento] [varchar](20) NULL,
	[tipodeDocumento] [varchar](50) NULL,
	[endereco] [varchar](100) NULL,
	[contato][varchar](30) NULL,
	[senha] [varchar](20) NULL,
	[email] [varchar](50) NULL,
	[filiacao] [varchar](50) NULL,
	[tipo] [varchar](50) NULL,
	[descricao] [varchar](50) NULL,
	[genero] [char](1) NULL,
	[dataCadastro] [datetime] NULL,
	[idEmpresa] [int] NULL,
	[indAtivo] [char](1) NULL
	CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED ([idUsuario] ASC)
);
GO

create table LogMultiPlatformCompany(
	[idLogMultiPlatformCompany] [int] IDENTITY(1,1) NOT NULL,
	[idUsuario] [int] NOT NULL,
	[login] [varchar](50) NOT NULL,
    [idSistema] [int] NOT NULL,
	[Metodo] [varchar](50) NOT NULL,
	[TipoErro] [char](1) NOT NULL,
	[documento] [varchar](20) NOT NULL,
	[Descricao] [varchar](max) NOT NULL,
	[dataLogSistema] [datetime] NOT NULL
	CONSTRAINT [PK_LogMultiPlatformCompany] PRIMARY KEY CLUSTERED ([idLogMultiPlatformCompany] ASC)
);
GO