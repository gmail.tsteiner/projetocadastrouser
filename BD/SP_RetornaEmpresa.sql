CREATE PROCEDURE SP_RetornaEmpresa(@idEmpresa int)
AS
BEGIN
SELECT [idEmpresa]
      ,[NomeEmpresa]
  FROM [dbo].[Empresa]
  where idEmpresa = @idEmpresa
END