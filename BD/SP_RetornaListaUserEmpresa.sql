CREATE PROCEDURE SP_RetornaListaUserEmpresa(@idEmpresa int)
As
Begin
SELECT idUsuario
      ,login
      ,nomeCadastrado
      ,dataCadastro
      ,(select nomeAcessoSistemaUsuario 
	   from TipoAcessoSistemaUsuario(nolock) t
	      inner join AcessoSistemaUsuario (nolock) a on a.idTipoAcessoSistemaUsuario = t.idTipoAcessoSistemaUsuario
		 where a.idUsuario = u.idUsuario) as nomeAcessoSistemaUsuario
  FROM Usuario (nolock) u 
  where [idEmpresa] = @idEmpresa
END