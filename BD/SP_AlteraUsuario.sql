CREATE PROCEDURE SP_AlteraUsuario(@idUsuario int,
                                  @nomeCadastrado varchar(50) =null,
								  @idade int =null,
							      @documento varchar(20) =null,
								  @tipodeDocumento varchar(50) =null,
								  @endereco varchar(100) =null,
								  @contato varchar(30) =null,
								  @email varchar(50) =null,
								  @filiacao varchar(50) =null,
								  @tipo varchar(50) =null,
								  @descricao varchar(50) =null,
								  @genero char(1) =null)
As
BEGIN

UPDATE Usuario
   set nomeCadastrado = @nomeCadastrado
      ,idade = @idade
      ,documento = @documento
      ,tipodeDocumento = @tipodeDocumento
      ,endereco = @endereco
      ,contato = @contato
	  ,email = @email
      ,filiacao = @filiacao
      ,tipo = @tipo
      ,descricao = @descricao
      ,genero = @genero
 WHERE idUsuario = @idUsuario

END